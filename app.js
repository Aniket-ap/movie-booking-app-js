// import { fetchMovieAvailability, fetchMovieList } from "./api.js";
// const mainSection = document.getElementById("main");

// async function renderAllMovies() {
//   mainSection.innerHTML = `<div id='loader'></div>`;
//   let movies = await fetchMovieList();
//   let movieHolder = document.createElement("div");
//   movieHolder.setAttribute("class", "movie-holder");
//   movies.forEach((movie) => {
//     createIndividualMovies(movie, movieHolder);
//   });
//   mainSection.innerHTML = "";
//   mainSection.appendChild(movieHolder);
// }

// renderAllMovies();

// function createIndividualMovies(movieData, wrapper) {
//   let a = document.createElement("a");
//   a.setAttribute("data-movie-name", `${movieData.name}`);
//   a.classList.add("movie-link");
//   a.href = `#${movieData.name}`;
//   a.innerHTML = `<div class="movie" data-id=${movieData.name}>
//                     <div class="movie-img-wrapper" style="background-image:url(${movieData.imgUrl})"></div>
//                     <h4>${movieData.name}</h4>
//                </div>`;
//   wrapper.appendChild(a);
// }

import { fetchMovieAvailability, fetchMovieList } from "./api.js";
$(document).ready(function () {
  const mainElement = document.getElementById("main");
  let bookerGridHolder = document.getElementById("booker-grid-holder");
  let bookTicketBtn = document.getElementById("book-ticket-btn");
  let booker = document.getElementById("booker");

  let user = {};

  $(
    `#register, #login , .nav-link.logout-click, #screen-ui, .seat-selection-guide, .user-email`
  ).hide();

  $(`.login-click`).click(function (e) {
    $(`#register, #jumbotron`).hide();
    $(`#login`).show();
  });

  $(`.register-click`).click(function (e) {
    $(`#login, #jumbotron`).hide();
    $(`#register`).show();
  });

  $(`#login-btn`).click(async function (e) {
    $("#login-btn").prop("disabled", true);
    let email = $(`#l-email`).val();
    let password = $(`#l-password`).val();
    if (email === "" || password === "") {
      alert("Email and Password should not be empty");
      return;
    }
    const response = await fetch(
      "https://basic-auth-nodejs-production.up.railway.app/login",
      {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: `${email}`,
          password: `${password}`,
        }),
      }
    );
    response.json().then((data) => {
      if (data.error) {
        alert(data.error);
        $("#login-btn").prop("disabled", false);
      } else {
        user.email = data.user.email;
        $(`.nav-link.login-click, .nav-link.register-click, #login`).hide();
        $(`.nav-link.logout-click, .user-email`).show();
        $(`.user-email`).text(user.email);
        alert(data.message);
        renderMovies();
      }
    });
  });

  $(`#signup-btn`).click(async function (e) {
    $("#signup-btn").prop("disabled", true);

    let email = $(`#r-email`).val();
    let password = $(`#r-password`).val();
    let name = $(`#r-name`).val();
    if (email === "" || password === "" || name === "") {
      alert("All input field is mandtory");
      return;
    }
    const response = await fetch(
      "https://basic-auth-nodejs-production.up.railway.app/register",
      {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: `${name}`,
          email: `${email}`,
          password: `${password}`,
        }),
      }
    );
    response.json().then((data) => {
      if (data.error) {
        alert(data.error);
        $("#signup-btn").prop("disabled", false);
      } else {
        $(`#register, .nav-link.logout-click`).hide();
        $(`#login `).show();
        alert(data.message);
      }
    });
  });

  $(`.navbar-brand, .home-click`).click(function (e) {
    if (!user?.email) {
      $(`.login-click`).trigger("click");
    }
  });

  $(`.nav-link.logout-click`).click(function (e) {
    $(`.nav-link.login-click, .nav-link.register-click`).show();
    $(`.nav-link.logout-click`).hide();
    location.reload();
  });

  async function renderMovies() {
    mainElement.innerHTML = `<div id='loader'></div>`;
    let movies = await fetchMovieList();
    let movieHolder = document.createElement("div");
    movieHolder.setAttribute("class", "movie-holder");
    movies.forEach((movie) => {
      createSeparateMovieTabs(movie, movieHolder);
    });
    mainElement.innerHTML = "";
    mainElement.appendChild(movieHolder);
    setEventToLinks();
  }
  //   renderMovies();

  function createSeparateMovieTabs(data, wrapper) {
    let a = document.createElement("div");
    a.setAttribute("data-movie-name", `${data.name}`);
    a.classList.add("movie-link", "mouse-pointer");
    a.href = `#${data.name}`;
    a.innerHTML = `<div class="movie" data-id=${data.name}>
                         <div class="movie-img-wrapper" style="background-image:url(${data.imgUrl})"></div>
                         <h4 class="text-center">${data.name}</h4>
                    </div>`;
    wrapper.appendChild(a);
  }

  function setEventToLinks() {
    let movieLinks = document.querySelectorAll(".movie-link");
    movieLinks.forEach((movieLink) => {
      movieLink.addEventListener("click", (e) => {
        renderSeatsGrid(movieLink.getAttribute("data-movie-name"));
      });
    });
  }

  async function renderSeatsGrid(movieName) {
    bookerGridHolder.innerHTML = `<div id='loader'></div>`;
    bookTicketBtn.classList.add("v-none");
    let data = await fetchMovieAvailability(movieName);
    renderSeats(data);
    setEventsToSeats();
  }

  function renderSeats(data) {
    if (booker.firstElementChild.tagName !== "H3") {
      seatsSelected = [];
    $(`#screen-ui, .seat-selection-guide`).show();
      booker.innerHTML = `<h3 class="v-none">Seat Selector</h3>
                                   <div id="booker-grid-holder">
                                   </div>
                                   <button id="book-ticket-btn" class="v-none btn btn-primary">Book my seats</button>`;
    }
    bookerGridHolder = document.getElementById("booker-grid-holder");
    bookTicketBtn = document.getElementById("book-ticket-btn");
    bookerGridHolder.innerHTML = "";
    booker.firstElementChild.classList.remove("v-none");
    $(`#screen-ui, .seat-selection-guide`).show();
    createSeatsGrid(data);
  }

  function createSeatsGrid(data) {
    let bookingGrid1 = document.createElement("div");
    let bookingGrid2 = document.createElement("div");
    bookingGrid2.classList.add("booking-grid");
    bookingGrid1.classList.add("booking-grid");
    for (let i = 1; i < 25; i++) {
      let seat = document.createElement("div");
      seat.innerHTML = i;
      seat.setAttribute("id", `booking-grid-${i}`);
      if (data.includes(i)) seat.classList.add("seat", "unavailable-seat");
      else seat.classList.add("seat", "available-seat");
      if (i > 12) bookingGrid2.appendChild(seat);
      else bookingGrid1.appendChild(seat);
    }
    bookerGridHolder.appendChild(bookingGrid1);
    bookerGridHolder.appendChild(bookingGrid2);
    setTicketBooking();
  }

  let seatsSelected = [];
  function setEventsToSeats() {
    let AvaliableSeats = document.querySelectorAll(".available-seat");
    AvaliableSeats.forEach((seat) => {
      seat.addEventListener("click", (_) => {
        saveSelectedSeat(seat);
      });
    });
  }

  function saveSelectedSeat(seat) {
    if (!seat.classList.contains("select-seat")) {
      seat.classList.add("select-seat");
      seatsSelected.push(seat.innerText);
      bookTicketBtn.classList.remove("v-none");
    } else {
      seat.classList.remove("select-seat");
      seatsSelected = seatsSelected.filter((item) => seat.innerText !== item);
      if (seatsSelected.length == 0) {
        bookTicketBtn.classList.add("v-none");
      }
    }
  }

  function setTicketBooking() {
    bookTicketBtn.addEventListener("click", () => {
      if (seatsSelected.length > 0) {
        booker.innerHTML = "";
        confirmTicket();
      }
    });
  }

  function confirmTicket() {
    let confirmTicketElement = document.createElement("div");
    confirmTicketElement.setAttribute("id", "confirm-purchase");
    let h3 = document.createElement("h3");
    h3.innerText = `You have selected ${seatsSelected.length} seats: ( ${seatsSelected.join(",")} ) for the total price of Rs.${seatsSelected.length * 250}`;
    confirmTicketElement.appendChild(h3);
    let btnClick = document.createElement("button");
    btnClick.classList.add("btn", "btn-primary")
    btnClick.setAttribute("id",  "confirm-btn")
    btnClick.textContent = "Confirm";
    confirmTicketElement.appendChild(btnClick);
    booker.appendChild(confirmTicketElement);
    success();
  }

  function success() {
    let submitBtn = document.getElementById("confirm-btn");
    submitBtn.addEventListener("click", (e) => {
     renderSuccessMessage();
    });
  }

  function renderSuccessMessage() {
    booker.innerHTML = "";
    createSuccessMessage();
  }

  function createSuccessMessage() {
    let successElement = document.createElement("div");
    successElement.setAttribute("id", "success");
    successElement.innerHTML = `<h3>Booking details</h3>
                                    <p>Seats: ${seatsSelected.join(", ")}</p>
                                   <p>Email: ${user.email}</p>`;
    booker.appendChild(successElement);
  }
});
